variable "vm_username" {
  description = "Azure VM username"
  type        = string
}

variable "vm_password" {
  description = "Azure VM password"
  type        = string
}
